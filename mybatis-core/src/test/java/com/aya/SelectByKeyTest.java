package com.aya;

import com.aya.mybatis.core.Blog;
import com.aya.mybatis.core.BlogMapper;
import org.apache.ibatis.builder.StaticSqlSource;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.*;
import org.apache.ibatis.session.*;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SelectByKeyTest {
    @Test
    public void testSelectByKeyBean() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Blog blog = new Blog();
            int id = 101;
            blog.setId(id);
            blog.setTitle("11");
            blog.setContent("22");
            int i = mapper.insertWithSelectByKey(blog);
            Blog blog1 = mapper.selectOneById(id);
            System.out.println("id:" + blog1.getId());
            System.out.println("line:" + i);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    /**
     * 动态创建 sql 添加到 mybatis 中
     *
     * @throws IOException
     */
    @Test
    public void testAutoMappedStatement() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        Configuration configuration = sqlSessionFactory.getConfiguration();
        SqlSource sqlSource = new StaticSqlSource(configuration, "select * from blog limit 10");

        MappedStatement.Builder builder = new MappedStatement.Builder(configuration, "autoTest", sqlSource, SqlCommandType.SELECT);
//        builder.resultSets("java.lang.String");
        ResultMapping.Builder resultMappingBuilder = new ResultMapping.Builder(configuration, "id", "id", String.class);
        ResultMap.Builder resultMapBuilder = new ResultMap.Builder(configuration, "autoQueryMapping", Blog.class, new ArrayList<ResultMapping>() {{
            add(resultMappingBuilder.build());
        }}, true);

        builder.resultMaps(new ArrayList<ResultMap>() {{
            ResultMap build = resultMapBuilder.build();
            add(build);
        }});
        MappedStatement mappedStatement = builder.build();
        configuration.addMappedStatement(mappedStatement);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            List<Blog> blogList = sqlSession.selectList("autoTest");
            System.out.println(blogList);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

}
