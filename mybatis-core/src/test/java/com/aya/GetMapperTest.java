package com.aya;

import com.aya.mybatis.core.Blog;
import com.aya.mybatis.core.BlogMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

public class GetMapperTest extends BaseBeforeTest {
    Logger logger = LoggerFactory.getLogger(GetMapperTest.class);

    @Test
    public void testRead() throws IOException {

        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(BaseBeforeTest.init());
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Set<Blog> blogs = mapper.selectAll();
            logger.info("blogs,{}", blogs);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testGetMapperWithConfiguration() throws IOException {


        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {

            BlogMapper mapper = sqlSession.getConfiguration().getMapper(BlogMapper.class, sqlSession);
            Set<Blog> blogs = mapper.selectAll();
            logger.info("blogs,{}", blogs);
        } finally {
            sqlSession.close();
        }
    }


}
