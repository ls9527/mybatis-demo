package com.aya;

import com.aya.mybatis.core.Blog;
import com.aya.mybatis.core.BlogMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class CoreTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void before() throws IOException, SQLException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void testRead() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            List<Blog> blog = sqlSession.selectList("com.aya.dead.mapper.BlogMapper.selectAll");
            System.out.println(blog);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testReadList() throws IOException {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Blog> objects = sqlSession.selectList("selectAll");
            System.out.println(objects);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReadWrapCollection() throws IOException {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            Object[] params = {"2", "3"};
            sqlSession.selectList("selectBlogInId", params);
            sqlSession.selectList("selectBlogInId", params);
            List<Object> objects = sqlSession.selectList("selectBlogInId", params);

            System.out.println(objects);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReadSet() throws IOException, SQLException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Set<Blog> blogs = mapper.selectSet();
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }


    @Test
    public void testSelectById() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            Blog blog = new Blog();
            blog.setContent("id");
            blog.setId(1);
            Blog blogs = sqlSession.selectOne("com.aya.dead.mapper.BlogMapper.selectBlog", blog);
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testReadRaram() throws IOException {


        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Blog blogs = mapper.selectByDate(new Date());
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

}
