package com.aya;

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.Before;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseBeforeTest {
    public static Connection init() {
        try {
            Connection connection =
                    DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");

            ScriptRunner scriptRunner = new ScriptRunner(connection);
            ClassLoader classLoader = BaseBeforeTest.class.getClassLoader();
            InputStream resourceAsStream = classLoader.getResourceAsStream("ddl/table.sql");
            scriptRunner.runScript(new InputStreamReader(resourceAsStream));
            return connection;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
