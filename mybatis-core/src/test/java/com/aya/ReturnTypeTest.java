package com.aya;

import com.aya.mybatis.core.Blog;
import com.aya.mybatis.core.BlogMapper;
import org.apache.ibatis.exceptions.ExceptionFactory;
import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.result.DefaultMapResultHandler;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.session.*;
import org.apache.ibatis.session.defaults.DefaultSqlSession;
import org.apache.ibatis.session.defaults.DefaultSqlSessionFactory;
import org.apache.ibatis.transaction.Transaction;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.managed.ManagedTransactionFactory;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;

public class ReturnTypeTest {

    @Test
    public void testSelectTreeSet() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            TreeSet<Blog> blogs = mapper.selectTreeSet();
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testDefaultSet() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Set<Blog> blogs = mapper.selectSet();
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testDefaultMap() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map<String, Blog> blogs = mapper.selectMap();
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    /**
     * map 类型无法使用自定义的类型
     *
     * @throws IOException
     */
    @Test
    public void testCustomLinkedHashMap() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map<String, Blog> blogs = mapper.selectLinkedHashMap();
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testCustomLinkedHashMapMapper() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map<String, Blog> blogs = new LinkedHashMap<>();
            Configuration configuration = sqlSessionFactory.getConfiguration();
            DefaultMapResultHandler defaultMapResultHandler = new DefaultMapResultHandler("id",
                    configuration.getObjectFactory(),
                    configuration.getObjectWrapperFactory(), configuration.getReflectorFactory()) {
                @Override
                public Map getMappedResults() {
                    return blogs;
                }
            };

            mapper.selectLinkedHashMapMapper(defaultMapResultHandler);
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testCustomTreeMapMapper() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            Map<String, Blog> blogs = new TreeMap<>();
            Configuration configuration = sqlSessionFactory.getConfiguration();
            DefaultMapResultHandler defaultMapResultHandler = new DefaultMapResultHandler("id",
                    configuration.getObjectFactory(),
                    configuration.getObjectWrapperFactory(), configuration.getReflectorFactory()) {
                @Override
                public Map getMappedResults() {
                    return blogs;
                }
            };

            mapper.selectLinkedHashMapMapper(defaultMapResultHandler);
            System.out.println(blogs);
        } finally {
            sqlSession.close();
        }
    }

    class AnoDefaultMapResultHandler<K, V> implements ResultHandler<V> {


        private final Map<K, V> mappedResults;
        private final String mapKey;
        private final ObjectFactory objectFactory;
        private final ObjectWrapperFactory objectWrapperFactory;
        private final ReflectorFactory reflectorFactory;

        public AnoDefaultMapResultHandler(String mapKey, ObjectFactory objectFactory, ObjectWrapperFactory objectWrapperFactory, ReflectorFactory reflectorFactory, Class<Map<K, V>> returnMapClass) {
            this.objectFactory = objectFactory;
            this.objectWrapperFactory = objectWrapperFactory;
            this.reflectorFactory = reflectorFactory;
            this.mappedResults = objectFactory.create(returnMapClass);
            this.mapKey = mapKey;
        }

        @Override
        public void handleResult(ResultContext<? extends V> context) {
            final V value = context.getResultObject();
            final MetaObject mo = MetaObject.forObject(value, objectFactory, objectWrapperFactory, reflectorFactory);
            // TODO is that assignment always true?
            final K key = (K) mo.getValue(mapKey);
            mappedResults.put(key, value);
        }

        public Map<K, V> getMappedResults() {
            return mappedResults;
        }
    }

    @Test
    public void testSqlSessionCustomMapMapper() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        sqlSessionFactory = new DefaultSqlSessionFactory(sqlSessionFactory.getConfiguration()) {

            @Override
            public SqlSession openSession() {
                Transaction tx = null;
                try {
                    TransactionIsolationLevel level = null;
                    Boolean autoCommit = false;
                    final Environment environment = getConfiguration().getEnvironment();
                    final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
                    tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);
                    final Executor executor = getConfiguration().newExecutor(tx, null);
                    return new DefaultSqlSession(getConfiguration(), executor, autoCommit) {
                        @Override
                        public <K, V> Map<K, V> selectMap(String statement, Object parameter, String mapKey, RowBounds rowBounds) {
                            Map<K, V> objectObjectMap = super.<K, V>selectMap(statement, parameter, mapKey, rowBounds);
                            LinkedHashMap<K, V> kvLinkedHashMap = new LinkedHashMap<>();
                            kvLinkedHashMap.putAll(objectObjectMap);
                            return kvLinkedHashMap;
                        }
                    };
                } catch (Exception e) {
                    closeTransaction(tx); // may have fetched a connection so lets call close()
                    throw ExceptionFactory.wrapException("Error opening session.  Cause: " + e, e);
                } finally {
                    ErrorContext.instance().reset();
                }
            }

            private TransactionFactory getTransactionFactoryFromEnvironment(Environment environment) {
                if (environment == null || environment.getTransactionFactory() == null) {
                    return new ManagedTransactionFactory();
                }
                return environment.getTransactionFactory();
            }

            private void closeTransaction(Transaction tx) {
                if (tx != null) {
                    try {
                        tx.close();
                    } catch (SQLException ignore) {
                        // Intentionally ignore. Prefer previous error.
                    }
                }
            }
        };
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
            LinkedHashMap<String, Blog> stringBlogLinkedHashMap = mapper.selectLinkedHashMap();
            System.out.println(stringBlogLinkedHashMap);
        } finally {
            sqlSession.close();
        }
    }
}
