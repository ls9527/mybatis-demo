package com.aya.plugin;

import com.aya.mybatis.core.Blog;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author by liushang@zsyjr.com
 * @Date 2019/6/4 11:12
 * @Created by liushang@zsyjr.com
 */
public class GeneratorTest {
    @Test
    public void testRead() throws IOException {


        String resource = "plugin/id/mybatis-config-id.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            Blog blog = new Blog();
            blog.setTitle("genTitle");
            blog.setContent("genContent");
            sqlSession.insert("com.aya.dead.mapper.BlogMapper.insert", blog);
            System.out.println(blog);
        } finally {
            sqlSession.close();
        }
    }
}
