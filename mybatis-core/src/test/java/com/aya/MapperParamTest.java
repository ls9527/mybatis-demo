package com.aya;

import com.aya.mybatis.core.Blog;
import com.aya.mybatis.core.BlogParamMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class MapperParamTest {
    Logger logger = LoggerFactory.getLogger(MapperParamTest.class);

    @Test
    public void testParamName() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogParamMapper mapper = sqlSession.getMapper(BlogParamMapper.class);
            Blog blog = mapper.selectBlog("1", "title1");
            logger.info("blog,{}", blog);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void testArgName() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            BlogParamMapper mapper = sqlSession.getMapper(BlogParamMapper.class);
            Blog blog = mapper.selectArgName("1", "title1");
            logger.info("blog,{}", blog);
        } finally {
            sqlSession.close();
        }
    }

}
