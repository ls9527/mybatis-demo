package com.aya;

import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.property.PropertyTokenizer;
import org.apache.ibatis.reflection.wrapper.BeanWrapper;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.junit.Test;

import java.util.*;

public class MetaObjectTest {
    @Test
    public void testBean() {
        Animal animal = new Animal();
        MetaObject metaObject = MetaObject.forObject(animal, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        metaObject.setValue("name", "bean");
        System.out.println(animal.getName());
    }


    @Test
    public void testCollection() {
        Animal animal = new Animal();
        animal.setName("collection");
        List<Animal> list = new ArrayList<>();
        MetaObject metaObject = MetaObject.forObject(list, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        metaObject.add(animal);

        System.out.println(list.get(0).getName());
    }


    @Test
    public void testCollectionGet() {
        Animal animal = new Animal();
        animal.setName("collection");
        List<Animal> list = new ArrayList<>();
        MetaObject metaObject = MetaObject.forObject(list, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        metaObject.add(animal);
        Animal convert = (Animal) metaObject.getValue("[0]");
        System.out.println(convert.getName());
    }

    @Test
    public void testCustomBeanWrapper() {
        Animal animal = new Animal();
        animal.setName("customBeanWrapper");
        List<Animal> list = new ArrayList<>();
        MetaObject metaObject = MetaObject.forObject(list, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        metaObject.add(animal);

        BeanWrapper beanWrapper = new BeanWrapper(metaObject, list);
        Animal convertAnimal = (Animal) beanWrapper.get(new PropertyTokenizer("[0]"));
        System.out.println(convertAnimal.getName());
    }


    @Test
    public void testMap() {
        Animal animal = new Animal();
        animal.setName("map");
        Map<String, Animal> map = new HashMap<>();
        MetaObject metaObject = MetaObject.forObject(map, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        metaObject.setValue("deer", animal);
        System.out.println(map.get("deer").getName());
    }

    // 分隔符赋值
    @Test
    public void testTokenizer() {
        Animal animal = new Animal();
        animal.setParent(new Animal());
        MetaObject metaObject = MetaObject.forObject(animal, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        metaObject.setValue("parent.name", "tokenizer");
        System.out.println(animal.getParent().getName());
    }

    @Test
    public void testHash() {
        MetaObject metaObject = MetaObject.forObject(null, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        Object hashcode = metaObject.getValue("package");
        System.out.println(hashcode);
    }

    @Test
    public void testClassInfo() {

        MetaObject metaObject = MetaObject.forObject(MyNullObject.class, new DefaultObjectFactory(), new DefaultObjectWrapperFactory(), new DefaultReflectorFactory());
        System.out.println(metaObject.getValue("typeName"));
    }

    private static class MyNullObject {
    }
}
