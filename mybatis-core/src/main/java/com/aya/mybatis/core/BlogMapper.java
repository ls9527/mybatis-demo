package com.aya.mybatis.core;
import org.apache.ibatis.annotations.Param;
import com.aya.mybatis.core.Blog;

import org.apache.ibatis.annotations.Flush;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.session.ResultHandler;

import java.util.*;

/**
 * @author lius
 */
public interface BlogMapper {
    Blog selectOneById(@Param("id") Integer id);


    /**
     * sd
     */
    Blog selectBlog(Blog blog);

    @Flush
    void flushCache();

    List<Blog> selectBlogInId(List<String> ids);

    Set<Blog> selectAll();

    TreeSet<Blog> selectTreeSet();

    Set<Blog> selectSet();

    int insertWithSelectByKey(Blog blog);

    Blog selectByDate(Date date);

    @MapKey("id")
    Map<String, Blog> selectMap();

    @MapKey("id")
    LinkedHashMap<String, Blog> selectLinkedHashMap();

    @MapKey("id")
    void selectLinkedHashMapMapper(ResultHandler resultHandler);
}

