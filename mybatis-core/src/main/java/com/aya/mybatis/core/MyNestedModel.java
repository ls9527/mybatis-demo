package com.aya.mybatis.core;

public class MyNestedModel {
    private String title;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MyNestedModel{");
        sb.append("title='").append(title).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
