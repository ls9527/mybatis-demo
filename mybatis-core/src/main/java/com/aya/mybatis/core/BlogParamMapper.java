package com.aya.mybatis.core;

/**
 * @author lius
 */
public interface BlogParamMapper {
    Blog selectBlog(String id, String title);

    Blog selectArgName(String id, String title);
}

