package com.aya.mybatis.core;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MySplitHandler implements TypeHandler<Object> {


    @Override
    public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
        // 不能作为参数进行查询
    }

    @Override
    public Object getResult(ResultSet rs, String columnName) throws SQLException {
        String string = rs.getString(columnName);
        if (string == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }

    @Override
    public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
        String string = rs.getString(columnIndex);
        if (string == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }

    @Override
    public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String string = cs.getString(columnIndex);
        if (string == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }
}
