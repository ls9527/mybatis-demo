# mybatis 源码分析
## 基本使用
[基本使用](/mybatis/doc/base/introduction.md)
## mybatis
按照以下步骤进行分析。
1. [创建 SqlSessionFactory](/mybatis/doc/base/createSqlSessionFactory.md)
2. [开启会话](/mybatis/doc/base/openSession.md)
3. [查询数据](/mybatis/doc/base/selectOne.md)
4. [关闭会话](/mybatis/doc/base/closeSession.md)

阅读完上述分析之后可以解答以下问题
1. 如何载入配置文件
2. 开启会话和关闭会话做了什么


## mybatis-spring
1. spring集成基本使用


# 参考
官方文档-快速开始 http://www.mybatis.org/mybatis-3/getting-started.html