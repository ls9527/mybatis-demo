create database mybatis;
use mybatis;

create table t_blog
(
    id      bigint auto_increment
        primary key,
    title   varchar(255) null,
    content varchar(255) null
);

INSERT INTO t_blog (id, title, content) VALUES (1, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (2, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (3, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (4, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (5, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (6, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (7, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (8, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (9, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (10, 'zzzz', 'aaaaa');
INSERT INTO t_blog (id, title, content) VALUES (11, 'zzzz', 'aaaaa');