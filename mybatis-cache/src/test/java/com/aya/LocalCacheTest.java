package com.aya;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class LocalCacheTest {


    /**
     * 一级缓存 生效测试
     *
     * @throws IOException
     */
    @Test
    public void testLocalCacheValid() throws IOException {
        String resource = "cache/local/valid/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Object> objectsA = sqlSession.selectList("com.aya.mapper.BlogMapper.selectAll");

            List<Object> objectsB = sqlSession.selectList("com.aya.mapper.BlogMapper.selectAll");

            // objectsB 内存获取  objectsA == objectsB
            Assert.assertTrue(objectsA == objectsB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 配置 flushCache="true"
     * 一级缓存失效测试
     *
     * @throws IOException
     */
    @Test
    public void testLocalCacheInvalid() throws IOException {
        String resource = "cache/local/invalid/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Object> objectsA = sqlSession.selectList("com.aya.mapper.BlogMapper.selectAll");

            List<Object> objectsB = sqlSession.selectList("com.aya.mapper.BlogMapper.selectAll");

            // objectsB 仍然从数据库读取  objectsA != objectsB
            Assert.assertTrue(objectsA != objectsB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
