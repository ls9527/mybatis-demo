package com.aya;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class GlobalCacheTest {


    /**
     * 二级缓存生效
     *
     * @throws IOException
     */
    @Test
    public void testGlobalCacheValid() throws IOException {
        String resource = "cache/global/valid/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        List<Object> objectsA, objectsB;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 配置 useCache="false"
     * 二级缓存 失效
     *
     * @throws IOException
     */
    @Test
    public void testGlobalCacheInvalid() throws IOException {
        String resource = "cache/global/invalid/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        List<Object> objectsA = null;
        List<Object> objectsB = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(objectsA != objectsB);
    }

    @Test
    public void testGlobalCacheReadOnly() throws IOException {
        String resource = "cache/global/readonly/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        List<Object> objectsA = null;
        List<Object> objectsB = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(objectsA == objectsB);
    }


    @Test
    public void testGlobalCacheCustom() throws IOException {
        String resource = "cache/global/custom/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        List<Object> objectsA = null;
        List<Object> objectsB = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(objectsA == objectsB);
    }


}
