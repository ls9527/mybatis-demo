package com.aya;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 四种二级缓存测试
 */
public class CacheTypeTest {


    /**
     * LRU 缓存
     *
     * @throws IOException
     */
    @Test
    public void testLruCache() throws IOException {
        String resource = "cache/type/lru/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        List<Object> objectsA, objectsB;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * LRU 缓存
     *
     * @throws IOException
     */
    @Test
    public void testFifoCache() throws IOException {
        String resource = "cache/type/fifo/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        List<Object> objectsA = null;
        List<Object> objectsB = null;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * LRU 缓存
     *
     * @throws IOException
     */
    @Test
    public void testSoftCache() throws IOException {
        String resource = "cache/type/soft/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        List<Object> objectsA, objectsB;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * LRU 缓存
     *
     * @throws IOException
     */
    @Test
    public void testWeakCache() throws IOException {
        String resource = "cache/type/weak/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        List<Object> objectsA, objectsB;
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            objectsA = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //命中全局缓存
            objectsB = sqlSession.selectList("selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
