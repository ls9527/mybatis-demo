package com.aya.cache.mapper;
import org.apache.ibatis.annotations.Param;
import com.aya.cache.mapper.Blog;

import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * @author lius
 * @Entity com.aya.cache.mapper.Blog
 */
@Alias("blogMapper")
public interface BlogMapper {
    /**
     * sd
     *
     * @param id id
     * @return Blog
     */
    Blog selectBlog(Integer id);


    List<Blog> selectAll();

    List<Blog> selectAllById(@Param("id") Integer id);
}
