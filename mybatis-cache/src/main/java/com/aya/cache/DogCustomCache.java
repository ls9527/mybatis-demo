package com.aya.cache;

import org.apache.ibatis.cache.Cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;

public class DogCustomCache implements Cache {

    String id;

    public DogCustomCache(String id) {
        this.id = id;
    }

    Map<Object, Object> map = new HashMap<>();

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void putObject(Object key, Object value) {
        map.put(key, value);
    }

    @Override
    public Object getObject(Object key) {
        return map.get(key);
    }

    @Override
    public Object removeObject(Object key) {
        return map.remove(key);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public int getSize() {
        return map.size();
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return null;
    }
}
