package com.aya;

import com.aya.dead.mapper.Blog;
import com.aya.dead.mapper.BlogMapper;
import org.apache.ibatis.annotations.Param;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

public class MybatisTest {

    public static final String INIT = "INIT";
    public static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";
    private static final String OTHER_INIT = "OTHER_INIT";

    @Test
    public void testSimple() {
        int page = 0;
        int size = 10;
        List<Blog> blogs = blogMapper.selectBlogListByPage(page, size);
        for (Blog blog : blogs) {
            // 处理blog数据
            System.out.println(blog.getTitle());
//            blogMapper.updateStatus(blog.getId(), INIT, SUCCESS);
        }
    }

    @Test
    public void testPage() {
        int page = 0;
        int size = 10;
        List<Blog> blogs;
        do {
            blogs = blogMapper.selectBlogListByPage(page * size, size);
            page++;
            // 处理blog数据
            for (Blog blog : blogs) {
                System.out.println(blog.getTitle());
            }
        } while (blogs.size() > 0);
    }

    BlogMapper blogMapper = null;

    @Before
    public void testBefore() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        blogMapper = applicationContext.getBean(BlogMapper.class);
    }

    @Test
    public void testPageChangeStatus() {
        int page = 0;
        int size = 2;
        List<Blog> blogs;
        do {
            blogs = blogMapper.selectBlogListByStateAndPage(Arrays.asList(INIT), page * size, size);
            page++;
            // 处理blog数据
            for (Blog blog : blogs) {
                blogMapper.updateStatus(blog.getId(), INIT, SUCCESS);
            }
        } while (blogs.size() > 0);
    }


    @Test
    public void testPageStepException() {
        int page = 0;
        int size = 2;
        List<Blog> blogs;
        do {
            blogs = blogMapper.selectBlogListByStateAndPage(Arrays.asList(INIT), page * size, size);
            // 处理blog数据

            for (Blog blog : blogs) {
                try {
                    // 这里模拟一个事物中出现异常被方法自己捕捉的场景
                    {
                        if ("title4".equals(blog.getTitle())) {
                            throw new RuntimeException("业务处理异常");
                        }
                        blogMapper.updateStatus(blog.getId(), INIT, SUCCESS);
                    }
                } catch (Exception e) {
                    // 在第一行更新状态为FAIL
                    // blogMapper.updateStatus(blog.getId(), INIT, FAIL);
                    e.printStackTrace();
                }
            }
        } while (blogs.size() > 0);
    }


    @Test
    public void testMultiStatusPage() {
        int page = 0;
        int size = 2;
        List<Blog> blogs;
        do {
            // 加入多条数据
            blogs = blogMapper.selectBlogListByStateAndPage(Arrays.asList(INIT, OTHER_INIT), page * size, size);
            // 处理blog数据

            for (Blog blog : blogs) {
                try {
                    // 这里模拟一个事物中出现异常被方法自己捕捉的场景
                    {
                        if ("title7".equals(blog.getTitle())) {
                            throw new RuntimeException("业务处理异常");
                        }
                        blogMapper.updateStatus(blog.getId(), INIT, SUCCESS);
                    }
                } catch (Exception e) {
                    // 在第一行更新状态为FAIL
                    blogMapper.updateStatus(blog.getId(), INIT, FAIL);
                    e.printStackTrace();
                }
            }
        } while (blogs.size() > 0);
    }
}
