package com.aya.dead.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author lius
 */
public interface BlogMapper {

    List<Blog> selectBlogListByStateAndPage(@Param("list") List<String> statusList, @Param("offset") Integer offset, @Param("size") Integer size);

    List<Blog> selectBlogListByPage(@Param("offset") Integer offset, @Param("size") Integer size);

    void updateStatus(@Param("id") String id, @Param("oldStatus") String oldStatus, @Param("newStatus") String newStatus);
}

