package com.aya;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class LogTest {


    /**
     * 二级缓存测试
     *
     * @throws IOException
     */
    @Test
    public void testLog() throws IOException {

        String resource = "log/log4j/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Object> objectsA = sqlSession.selectList("com.aya.mapper.BlogMapper.selectAll");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
