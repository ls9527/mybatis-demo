# mybatis的基本使用

## maven配置
```
 <!-- https://mvnrepository.com/artifact/org.mybatis/mybatis -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.4.6</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.41</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        
        
```

## mybatis-config.xml 配置
```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <properties resource="config.properties">
    </properties>
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>
    <mappers>
        <mapper resource="mapper/UserMapper.xml"/>
    </mappers>
</configuration>
```

## config.properties 配置
```
driver=com.mysql.jdbc.Driver
url: jdbc:mysql://localhost:3306/mybatis?useUnicode=true&characterEncoding=utf-8
username: root
password: root
```

## UserMapper.xml 配置
```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.aya.mapper.BlogMapper">
    <select id="selectBlog" resultType="com.aya.mapper.Blog">
        select * from blog where id = #{id}
    </select>
</mapper>
```


## sql 配置
```
CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

### 导入一条测试数据
```
INSERT INTO `mybatis`.`blog` (`id`, `title`, `content`) VALUES ('1', '标题', '这篇博客的内容');
```

## 使用mybatis
```
    @Test
    public void testRead() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        Blog blog = null;
        try {
            blog = sqlSession.selectOne("com.aya.mapper.BlogMapper.selectBlog",1);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            sqlSession.close();
        }

        System.out.println(blog);
    }
```

执行测试, 控制台输出
```
Blog{id=1, title='标题', content='这篇博客的内容'}
```