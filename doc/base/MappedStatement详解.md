# mybatis MappedStatement 详解


在进行了解之前,先了解mybatis的配置体系:

![Configuration思维导图](http://cdn.blog.shangwantong.com/mybatis/xmind/Configuration.png)

图中并没有发现我们本章要讲述的: MappedStatement 

MappedStatement 定义: mybatis 定义的一个与具体操作 `select|insert|update|delete` 的类.

> 当mybatis加载 `configuration>mappers` 时,解析mapper子节点,只有CRUD这4种标签才会创建`MappedStatement`对象,添加到configuration中

# 分析
首先从`解析xml文件的configuration节点`开始
```
  private void configurationElement(XNode context) {
    try {
      String namespace = context.getStringAttribute("namespace");
      if (namespace == null || namespace.equals("")) {
        throw new BuilderException("Mapper's namespace cannot be empty");
      }
      builderAssistant.setCurrentNamespace(namespace);
      //解析标签 cache-ref
      cacheRefElement(context.evalNode("cache-ref"));
      //解析标签 cache
      cacheElement(context.evalNode("cache"));
      //解析标签 parameterMap
      parameterMapElement(context.evalNodes("/mapper/parameterMap"));
      //解析标签 resultMap
      resultMapElements(context.evalNodes("/mapper/resultMap"));
      //解析标签 sql
      sqlElement(context.evalNodes("/mapper/sql"));
      //解析标签 select|insert|update|delete
      buildStatementFromContext(context.evalNodes("select|insert|update|delete"));
    } catch (Exception e) {
      throw new BuilderException("Error parsing Mapper XML. The XML location is '" + resource + "'. Cause: " + e, e);
    }
  }
```

这里可以发现,mapper的9大标签都在这里解析

## 
```
  private void buildStatementFromContext(List<XNode> list) {
    if (configuration.getDatabaseId() != null) {
      buildStatementFromContext(list, configuration.getDatabaseId());
    }
    buildStatementFromContext(list, null);
  }

  private void buildStatementFromContext(List<XNode> list, String requiredDatabaseId) {
    for (XNode context : list) {
      final XMLStatementBuilder statementParser = new XMLStatementBuilder(configuration, builderAssistant, context, requiredDatabaseId);
      try {
        statementParser.parseStatementNode();
      } catch (IncompleteElementException e) {
        configuration.addIncompleteStatement(statementParser);
      }
    }
  }
```