

# 例子

```xml
    <resultMap id="BlogMap" type="com.aya.mapper.Blog"  >
         <id column="id" property="id"/>
         <result column="content" property="content"/>
    </resultMap>
```

```java

public class Blog implements Serializable{
    private Integer id;
    private String content;
    // 省略 getter,setter
}
```

执行查询结果:
```data
Blog{id=1,  content='c1,c2,c3,c4'}
```


# 需求
将查询的`content`字段用逗号分割,变成`List<String>`类型的数据。
# 转换
```java

public class Blog implements Serializable{
    private Integer id;
    private List<String> content;
    // 省略 getter,setter
}
```

那么我们只要定义一个TypeHandler 然后在设置到字段解析上就可以了
```xml
    <resultMap id="BlogMap" type="com.aya.mapper.Blog"  >
         <id column="id" property="id"/>
         <result column="content" property="content" typeHandler="com.aya.mapper.StringSplitHandler"/>
    </resultMap>
```

StringSplitHandler 内容如下
```java
public class MySplitHandler implements TypeHandler<Object> {


    @Override
    public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
        // 不能作为参数进行查询
    }

    @Override
    public Object getResult(ResultSet rs, String columnName) throws SQLException {
        String string = rs.getString(columnName);
        if(string==null){
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }

    @Override
    public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
        String string = rs.getString(columnIndex);
        if(string==null){
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }

    @Override
    public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String string = cs.getString(columnIndex);
        if(string==null){
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }
}


```

控制台输出
```data
Blog{id=1, contents=[c1, c2, c3, c4]}
```

成功将`String`转化为`List`类型

