# 创建SqlSessionFactory

## 前提知识
1. 了解java原生的xml解析方式
## 简介
```
  SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
```

就得解决下面三个问题
1. 接口的实现类是什么
2. 如何创建 Configuration
3. Configuration的数据来源


## 第一节 接口的实现类是什么

在这里实际上分为两步
1. 用 XMLConfigBuilder 构建 Configuration 对象
2. 通过Configuration对象,返回 DefaultSqlSessionFactory
```
   public SqlSessionFactory build(InputStream inputStream) {
    return build(inputStream, null, null);
  }
  
  public SqlSessionFactory build(InputStream inputStream, String environment, Properties properties) {
      try {
        XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, environment, properties);
        return build(parser.parse());
      } catch (Exception e) {
        throw ExceptionFactory.wrapException("Error building SqlSession.", e);
      } finally {
        ErrorContext.instance().reset();
        try {
          inputStream.close();
        } catch (IOException e) {
          // Intentionally ignore. Prefer previous error.
        }
      }
   }
   
   public SqlSessionFactory build(Configuration config) {
       return new DefaultSqlSessionFactory(config);
   }
```

到这里已经知道了接口的实现类: DefaultSqlSessionFactory

但是内容Configuration是通过 XMLConfigBuilder.parse 获得的


## 第二节 Configuration 如何创建
要分析内部的数据,就是分析**XMLConfigBuilder.parse 如何创建  Configuration** 

### 2.1 创建 XMLConfigBuilder
> XMLConfigBuilder parser = new XMLConfigBuilder(inputStream, environment, properties);
![XMLConfigBuilder类图](http://cdn.blog.shangwantong.com/mybatis/XPathParser.png)

给私有字段赋值,为parse 做准备
1. 初始化 XPathParser (**document**,**xpath**)
2. 初始化 BaseBuilder(**Configuration**)
3. 初始化 XMLConfigBuilder

在BaseBuilder构造时,创建了 Configuration 对象

### 2.2 获得Configuration
>  parser.parse()

XMLConfigBuilder
```
public Configuration parse() {
    if (parsed) {
      throw new BuilderException("Each XMLConfigBuilder can only be used once.");
    }
    parsed = true;
    parseConfiguration(parser.evalNode("/configuration"));
    return configuration;
  }
```

parser.evalNode("/configuration") 获取根节点 configuration 对象
 
parseConfiguration 根据XmlNode对象的数据填充 configuration 的数据

## 第三节 Configuration的数据来源
>  parseConfiguration(parser.evalNode("/configuration"));



解析配置
```
private void parseConfiguration(XNode root) {
    try {
      //issue #117 read properties first
      propertiesElement(root.evalNode("properties"));
      Properties settings = settingsAsProperties(root.evalNode("settings"));
      loadCustomVfs(settings);
      typeAliasesElement(root.evalNode("typeAliases"));
      pluginElement(root.evalNode("plugins"));
      objectFactoryElement(root.evalNode("objectFactory"));
      objectWrapperFactoryElement(root.evalNode("objectWrapperFactory"));
      reflectorFactoryElement(root.evalNode("reflectorFactory"));
      settingsElement(settings);
      // read it after objectFactory and objectWrapperFactory issue #631
      environmentsElement(root.evalNode("environments"));
      databaseIdProviderElement(root.evalNode("databaseIdProvider"));
      typeHandlerElement(root.evalNode("typeHandlers"));
      mapperElement(root.evalNode("mappers"));
    } catch (Exception e) {
      throw new BuilderException("Error parsing SQL Mapper Configuration. Cause: " + e, e);
    }
  }
```

到这里就已经根据 root 节点填充了 configuration 的所有内容了

![Configuration类图](http://cdn.blog.shangwantong.com/mybatis/Configuration.png)

# 节点解析
## properties
>   propertiesElement(root.evalNode("properties"));
```java

  private void propertiesElement(XNode context) throws Exception {
    if (context != null) {
      Properties defaults = context.getChildrenAsProperties();
      String resource = context.getStringAttribute("resource");
      String url = context.getStringAttribute("url");
      if (resource != null && url != null) {
        throw new BuilderException("The properties element cannot specify both a URL and a resource based property file reference.  Please specify one or the other.");
      }
      if (resource != null) {
        defaults.putAll(Resources.getResourceAsProperties(resource));
      } else if (url != null) {
        defaults.putAll(Resources.getUrlAsProperties(url));
      }
      Properties vars = configuration.getVariables();
      if (vars != null) {
        defaults.putAll(vars);
      }
      // 设置资源对象,给接下来的解析使用
      parser.setVariables(defaults);
      configuration.setVariables(defaults);
    }
  }

```

这一步的核心内容就是 configuration.setVariables(defaults);
1. 获取是否有子节点,有子节点就加入到 `defaults` 里面
2. 获取 resource 配置
3. 获取 url 配置
4. url 和 resource 不能同时存在的判断
5. 将 url或者resource中的内容加入到 `defaults`
6. 获取配置原先的配置,并加入到 `defaults`
7. 设置 parser 和 configuration 的 setVariables

根据 Properties 的特性,后加入的会覆盖之前加入的

那么覆盖顺序如下
1. 载入子节点
2. 载入资源/url
3. 载入 SqlSessionFactoryBuilder .build 时候的 properties(本文没有传入properties)

官方原文: http://www.mybatis.org/mybatis-3/zh/configuration.html#properties

## environments
```
private void environmentsElement(XNode context) throws Exception {
    if (context != null) {
      if (environment == null) {
        environment = context.getStringAttribute("default");
      }
      for (XNode child : context.getChildren()) {
        String id = child.getStringAttribute("id");
        if (isSpecifiedEnvironment(id)) {
          TransactionFactory txFactory = transactionManagerElement(child.evalNode("transactionManager"));
          DataSourceFactory dsFactory = dataSourceElement(child.evalNode("dataSource"));
          DataSource dataSource = dsFactory.getDataSource();
          Environment.Builder environmentBuilder = new Environment.Builder(id)
              .transactionFactory(txFactory)
              .dataSource(dataSource);
          configuration.setEnvironment(environmentBuilder.build());
        }
      }
    }
}
```

参数XNode对象的内容:
```xml
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <property name="driver" value="${driver}"/>
                <property name="url" value="${url}"/>
                <property name="username" value="${username}"/>
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>
```
这里要做的是 载入environment.id == environments.default 的element

1. 遍历environments子节点 environment ,找到指定的 environment 节点
2. 解析 environment 节点的 transactionManager 和 dataSource 节点
3. 设置 configuration 的  environment 字段

官方原文: http://www.mybatis.org/mybatis-3/zh/configuration.html#environments