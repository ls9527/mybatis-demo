# 查询列表数据
```
    @Override
      public <E> List<E> selectList(String statement) {
        return this.selectList(statement, null);
    }
    
    @Override
    public <E> List<E> selectList(String statement, Object parameter) {
        return this.selectList(statement, parameter, RowBounds.DEFAULT);
    }
      
    @Override
    public <E> List<E> selectList(String statement, Object parameter, RowBounds rowBounds) {
        try {
            MappedStatement ms = configuration.getMappedStatement(statement);
            return executor.query(ms, wrapCollection(parameter), rowBounds, Executor.NO_RESULT_HANDLER);
        } catch (Exception e) {
            throw ExceptionFactory.wrapException("Error querying database.  Cause: " + e, e);
        } finally {
            ErrorContext.instance().reset();
        }
    }
```

查询列表数据时,重载了 selectList 方法. 最终都是执行3个参数方法

1. 通过 configuration的mappedStatements 获取对应的 MappedStatement
2. [包装集合](#将集合或数组包装为map).
2. executor去执行查询, (executor在 openSession 时创建)

接下来分析,如何执行查询

## 第一节 如何执行查询
```
  @Override
  public <E> List<E> query(MappedStatement ms, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException {
    BoundSql boundSql = ms.getBoundSql(parameterObject);
    CacheKey key = createCacheKey(ms, parameterObject, rowBounds, boundSql);
    return query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
  }
```
### 全局缓存/二级缓存
```
 @Override
  public <E> List<E> query(MappedStatement ms, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql)
      throws SQLException {
    Cache cache = ms.getCache();
    if (cache != null) {
      flushCacheIfRequired(ms);
      if (ms.isUseCache() && resultHandler == null) {
        ensureNoOutParams(ms, boundSql);
        @SuppressWarnings("unchecked")
        List<E> list = (List<E>) tcm.getObject(cache, key);
        if (list == null) {
          list = delegate.<E> query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
          tcm.putObject(cache, key, list); // issue #578 and #116
        }
        return list;
      }
    }
    return delegate.<E> query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
  }
```
1. 获得缓存
用代理执行器 delegate.query

tcm.putObject BUG修复
- https://github.com/mybatis/mybatis-3/issues/116
- https://github.com/mybatis/mybatis-3/issues/578
## 第二节 代理执行器
### 一级缓存获取+延迟加载
```
  @SuppressWarnings("unchecked")
  @Override
  public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    ErrorContext.instance().resource(ms.getResource()).activity("executing a query").object(ms.getId());
    if (closed) {
      throw new ExecutorException("Executor was closed.");
    }
    if (queryStack == 0 && ms.isFlushCacheRequired()) {
      clearLocalCache();
    }
    List<E> list;
    try {
      queryStack++;
      list = resultHandler == null ? (List<E>) localCache.getObject(key) : null;
      if (list != null) {
        handleLocallyCachedOutputParameters(ms, key, parameter, boundSql);
      } else {
        list = queryFromDatabase(ms, parameter, rowBounds, resultHandler, key, boundSql);
      }
    } finally {
      queryStack--;
    }
    if (queryStack == 0) {
      for (DeferredLoad deferredLoad : deferredLoads) {
        deferredLoad.load();
      }
      // issue #601
      deferredLoads.clear();
      if (configuration.getLocalCacheScope() == LocalCacheScope.STATEMENT) {
        // issue #482
        clearLocalCache();
      }
    }
    return list;
  }
```
### 一级缓存添加 
```
  private <E> List<E> queryFromDatabase(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    List<E> list;
    localCache.putObject(key, EXECUTION_PLACEHOLDER);
    try {
      list = doQuery(ms, parameter, rowBounds, resultHandler, boundSql);
    } finally {
      localCache.removeObject(key);
    }
    localCache.putObject(key, list);
    if (ms.getStatementType() == StatementType.CALLABLE) {
      localOutputParameterCache.putObject(key, parameter);
    }
    return list;
  }
```
将查询的结果添加到一级缓存 localCache 中

### 执行查询
```
  @Override
  public <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
    Statement stmt = null;
    try {
      Configuration configuration = ms.getConfiguration();
      StatementHandler handler = configuration.newStatementHandler(wrapper, ms, parameter, rowBounds, resultHandler, boundSql);
      stmt = prepareStatement(handler, ms.getStatementLog());
      return handler.<E>query(stmt, resultHandler);
    } finally {
      closeStatement(stmt);
    }
  }
```
标准的JDBC查询
```
try{
    StatementHandler handler =  获得StatementHandler;
    return handler.executeQuery();// 返回查询内容 
}finally{
    handler.close();    // 关闭 StatementHandler
}
```

## 第三节 查询深度分析
### 分析 handler.<E>query(stmt, resultHandler);
```
  @Override
  public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
    return delegate.<E>query(statement, resultHandler);
  }
```
### 分析 delegate.<E>query(statement, resultHandler);
```
   @Override
   public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
     PreparedStatement ps = (PreparedStatement) statement;
     ps.execute();
     return resultSetHandler.<E> handleResultSets(ps);
   }
```
### 分析 resultSetHandler.<E> handleResultSets(ps);
```
   @Override
     public List<Object> handleResultSets(Statement stmt) throws SQLException {
       ErrorContext.instance().activity("handling results").object(mappedStatement.getId());
   
       final List<Object> multipleResults = new ArrayList<Object>();
   
       int resultSetCount = 0;
       ResultSetWrapper rsw = getFirstResultSet(stmt);
   
       List<ResultMap> resultMaps = mappedStatement.getResultMaps();
       int resultMapCount = resultMaps.size();
       validateResultMapsCount(rsw, resultMapCount);
       while (rsw != null && resultMapCount > resultSetCount) {
         ResultMap resultMap = resultMaps.get(resultSetCount);
         handleResultSet(rsw, resultMap, multipleResults, null);
         rsw = getNextResultSet(stmt);
         cleanUpAfterHandlingResultSet();
         resultSetCount++;
       }
   
       String[] resultSets = mappedStatement.getResultSets();
       if (resultSets != null) {
         while (rsw != null && resultSetCount < resultSets.length) {
           ResultMapping parentMapping = nextResultMaps.get(resultSets[resultSetCount]);
           if (parentMapping != null) {
             String nestedResultMapId = parentMapping.getNestedResultMapId();
             ResultMap resultMap = configuration.getResultMap(nestedResultMapId);
             handleResultSet(rsw, resultMap, null, parentMapping);
           }
           rsw = getNextResultSet(stmt);
           cleanUpAfterHandlingResultSet();
           resultSetCount++;
         }
       }
   
       return collapseSingleResultList(multipleResults);
     }
```
## 包装集合

```
  private Object wrapCollection(final Object object) {
    if (object instanceof Collection) {
      StrictMap<Object> map = new StrictMap<Object>();
      map.put("collection", object);
      if (object instanceof List) {
        map.put("list", object);
      }
      return map;
    } else if (object != null && object.getClass().isArray()) {
      StrictMap<Object> map = new StrictMap<Object>();
      map.put("array", object);
      return map;
    }
    return object;
  }
``` 
```
public static class StrictMap<V> extends HashMap<String, V> {

    private static final long serialVersionUID = -5741767162221585340L;

    @Override
    public V get(Object key) {
      if (!super.containsKey(key)) {
        throw new BindingException("Parameter '" + key + "' not found. Available parameters are " + this.keySet());
      }
      return super.get(key);
    }

  }
```
当object实现了接口 Collection 或者是一个数组的时候,就返回一个StrictMap(key不存在时,抛出异常)
