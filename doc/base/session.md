# mybatis session


从源码的角度,去探索mybatis session的内容
```java
    SqlSession sqlSession = sqlSessionFactory.openSession();
    try {
      sqlSession.doXXX
    } finally {
        sqlSession.close();
    }
```
本节搞清楚两个问题
1. openSession 创建的对象有什么
2. close 关闭了什么
# 打开会话
直接去 DefaultSqlSessionFactory 去跟进 openSession 的内容:
```
  public SqlSession openSession() {
    return openSessionFromDataSource(configuration.getDefaultExecutorType(), null, false);
  }
```
转发给 openSessionFromDataSource
```
 private SqlSession openSessionFromDataSource(ExecutorType execType, TransactionIsolationLevel level, boolean autoCommit) {
    Transaction tx = null;
    try {
      final Environment environment = configuration.getEnvironment();
      final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
      tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);
      final Executor executor = configuration.newExecutor(tx, execType);
      return new DefaultSqlSession(configuration, executor, autoCommit);
    } catch (Exception e) {
      closeTransaction(tx); // may have fetched a connection so lets call close()
      throw ExceptionFactory.wrapException("Error opening session.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
  }
```
内容如下图

![会话内容](http://cdn.blog.shangwantong.com/mybatis/sessioncontent.png)

实际上,这里就是一个`事务`和`缓存`的管理器,执行时做事务处理和缓存处理
# 关闭会话
```
 @Override
  public void close() {
  // 省略无关代码
      executor.close(isCommitOrRollbackRequired(false));
  }
```
跟进执行器的关闭
```
// 省略无关代码
    public void close(boolean forceRollback) {
        rollback(forceRollback);
        transaction.close();
    }
    
     public void rollback(boolean required) throws SQLException {
            clearLocalCache();
            if (required) {
              transaction.rollback();
            }
     }
     
      public void clearLocalCache() {
           localCache.clear();
           localOutputParameterCache.clear();
      }
```

不难看出,关闭会话实际上关闭了什么
1. 关闭事务
2. 清空一级缓存

# 总结
最终得出结论: 

mybatis 的session 就是一个事务和缓存的管理器.