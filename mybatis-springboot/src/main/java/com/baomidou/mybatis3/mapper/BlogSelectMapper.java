package com.baomidou.mybatis3.mapper;
import java.util.List;


import com.baomidou.mybatis3.alias.AliasBlog;
import com.baomidou.mybatis3.domain.Blog;
import org.apache.ibatis.annotations.Param;


public interface BlogSelectMapper {

    int insertSelective(Blog blog);

    Blog selectOneById(@Param("id")Long id);

    AliasBlog selectAliasOneById(@Param("id")Long id);


    List<Blog> selectByIdAndContent(@Param("id")Long id,@Param("content")String content);
}
