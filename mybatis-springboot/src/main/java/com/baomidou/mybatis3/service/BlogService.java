package com.baomidou.mybatis3.service;

import com.baomidou.mybatis3.domain.Blog;
import com.baomidou.mybatis3.mapper.BlogSelectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author liushang@zsyjr.com
 */
@Service
public class BlogService {
    @Resource
    private BlogSelectMapper blogSelectMapper;

    @Transactional
    public Blog selectOneById(Long id) {
        return blogSelectMapper.selectOneById(id);
    }
}
