package com.baomidou.mybatis3.service;

import com.baomidou.mybatis3.MybatisPlus3Application;
import com.baomidou.mybatis3.domain.Blog;
import com.baomidou.mybatis3.mapper.BlogSelectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest(classes = MybatisPlus3Application.class)
public class BlogMapperTest {


    @Resource
    private BlogSelectMapper blogSelectMapper;

    @Test
    void selectOneById() {
        final List<Blog> aaa = blogSelectMapper.selectByIdAndContent(1L, "aaa");
        System.out.println(aaa);
    }

    @Test
    void insertAndSelect() {
        Blog blog = new Blog();
        long id = 100L;
        blog.setId(id);
        blog.setAge(10);
        blog.setTitle("张三");
        blogSelectMapper.insertSelective(blog);
        Blog blogFromDB = blogSelectMapper.selectOneById(id);
        System.out.println(blogFromDB);
    }
}