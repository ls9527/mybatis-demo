package com.baomidou.mybatis3.service;

import com.baomidou.mybatis3.MybatisPlus3Application;
import com.baomidou.mybatis3.domain.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author liushang@zsyjr.com
 */
@SpringBootTest(classes = MybatisPlus3Application.class)
class BlogServiceTest {

    @Resource
    private BlogService blogService;


    @Test
    void selectOneById() {
        Blog blog = blogService.selectOneById(1L);
        System.out.println(blog);
    }


}