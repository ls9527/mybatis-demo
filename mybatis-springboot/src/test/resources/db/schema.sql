create table t_blog(
    id int(11) primary key not null,
    title varchar(40) ,
    content varchar(40) ,
    money decimal (21,9),
    age int(11)
);

insert  into t_blog(id,title,content,money,age)values(1,'title-1','content-1',10,30);