package com.aya;

import com.aya.mybaits.spring.domain.Blog;
import com.aya.mybaits.spring.mapper.BlogMapper;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ReadTest {

    @Test
    public void testAopLog()  {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BlogMapper blogMapper = applicationContext.getBean(BlogMapper.class);
        Blog blog = new Blog();
        int id = 107;
        blog.setId(id);
        blog.setTitle("hello");
        blog.setContent("content1");
        int i = blogMapper.insertAll(blog);

        Blog blog1 = blogMapper.selectBlog(id);

        System.out.println(blog1);

    }

    @Test
    public void testSelectOne()  {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BlogMapper blogMapper = applicationContext.getBean(BlogMapper.class);
        int id = 107;
        Blog blog1 = blogMapper.selectBlog(id);

        System.out.println(blog1);

    }

}
