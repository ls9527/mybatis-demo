package com.aya;

import com.aya.mybaits.spring.service.BlogService;
import com.aya.mybaits.spring.domain.Blog;
import com.aya.mybaits.spring.mapper.BlogMapper;
import org.h2.tools.RunScript;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AopTest {


    @Test
    public void cast(){
        final boolean assignableFrom = Iterable.class.isAssignableFrom(ArrayList.class);
        System.out.println(assignableFrom);
    }
    @Test
    public void testAopLog() throws SQLException, IOException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BlogService blogService = applicationContext.getBean(BlogService.class);

        final DataSource bean1 = applicationContext.getBean(DataSource.class);
        final Connection connection = bean1.getConnection();
        final String file = Thread.currentThread().getContextClassLoader().getResource("db/schema.sql").getFile();
        try (FileReader reader = new FileReader(file)) {
            RunScript.execute(connection, reader);
        }

        blogService.executeAll();
    }

    @Test
    public void testBlogMapper() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BlogMapper blogMapper = applicationContext.getBean(BlogMapper.class);
        final List<Blog> blogs = blogMapper.selectAllById(2, null);
        for (Blog blog : blogs) {
            System.out.println(blog);
        }
    }


    @Test
    public void testBlogById() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BlogMapper blogMapper = applicationContext.getBean(BlogMapper.class);
        final List<Blog> blogs = blogMapper.selectAllByTitleAndContent("title", "content");
        System.out.println(blogs);
    }

}
