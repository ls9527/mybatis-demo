package com.aya.mybaits.spring.service;

import com.aya.mybaits.spring.domain.Blog;
import com.aya.mybaits.spring.mapper.BlogMapper;
//import org.apache.ibatis.cursor.Cursor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BlogService {
    @Resource
    private BlogMapper blogMapper;

    /**
     * mysql的  jdbc 链接字符串(useCursorFetch=true)一定要加入这个配置才能保证是用游标进行查询的
     *
     * 在mysql的jdbc驱动中 com.mysql.jdbc.ResultSetImpl#next() 当resultSet执行next时,
     * 通过观察成员变量 rowData 的类型(RowDataCursor)来判断是不是游标查询
     *
     * RowData 有三种实现，静态，游标，动态
     * 游标的方式只有在开启jdbc链接字符串的配置（useCursorFetch=true）， 才有可能使用
     * 通过mapper返回一个游标，并且设置fetchSize即可
     *
     * 如果是游标查询, 会进入以下方法
     * com.mysql.jdbc.MysqlIO#fetchRowsViaCursor(java.util.List, long, com.mysql.jdbc.Field[], int, boolean)
     *
     * 参考: https://cdmana.com/2021/01/20210107030556164m.html
     */
    @Transactional
    public void executeAll(){

        blogMapper.updateTitleAndContentById("TITLE-M", "CONTENT-N", "1");

//        final Cursor<Blog> blogs1 = blogMapper.selectAll();
//        for (Blog blog : blogs1) {
//            System.out.println(blog);
//        }

    }

    public List<Blog> selectAllById(int size, String id) {
        return blogMapper.selectAllById(size, id);
    }
}
