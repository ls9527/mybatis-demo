package com.aya.mybaits.spring;

import com.aya.mybaits.spring.service.BlogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class MybatisConfigApp {
    static Logger logger = LoggerFactory.getLogger(MybatisConfigApp.class);

    public static void main(String[] args) {
        ApplicationContext appletContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BlogService bean = appletContext.getBean(BlogService.class);
        bean.executeAll();
//        BlogMapper bean2 = appletContext.getBean(BlogMapper.class);
//        final List<Blog> blogs = bean2.selectAllById(5, null);
//        for (Blog blog : blogs) {
//            System.out.println(blog);
//        }
//
//        bean2.updateTitleAndContentById("TITLE-M", "CONTENT-N", "1");
    }
}
