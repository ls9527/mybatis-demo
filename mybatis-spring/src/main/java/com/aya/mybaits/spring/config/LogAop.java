package com.aya.mybaits.spring.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
//@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LogAop {

    @Pointcut("execution(* com.aya..*.*(..))")
    public void mapper() {

    }

    @Around("mapper()")
    public Object performance(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        String className = proceedingJoinPoint.getTarget().getClass().getName();
        Signature s = proceedingJoinPoint.getSignature();
        MethodSignature ms = (MethodSignature) s;
        Method m = ms.getMethod();
        String methodName = m.getName();

        System.out.println("aop:" + className + "." + methodName);
        return proceedingJoinPoint.proceed();
    }
}
